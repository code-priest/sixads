from django.apps import AppConfig


class YoutubeScraperConfig(AppConfig):
    name = 'applications.youtube_scraper'

    def ready(self):
        super().ready()
        from . import celery_tasks
