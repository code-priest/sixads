from django.conf import settings

from sixads.celery import celery

from .scraper import scrap


@celery.task
def run_scraper():
    channel_name = settings.YOUTUBE_SCRAPER_TARGET_CHANNEL
    api_key = settings.YOUTUBE_SCRAPER_API_KEY
    scrap(api_key, channel_name)
