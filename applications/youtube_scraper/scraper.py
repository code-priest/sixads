import logging
import os
from django.utils import timezone

import googleapiclient.discovery

from . import models


def scrap(api_key, channel_name):
    """
    Scraps youtube channel's statistic.
    """
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
    youtube = googleapiclient.discovery.build(
        'youtube',
        'v3',
        developerKey=api_key,
        cache_discovery=False
    )

    response = youtube.channels().list(
        part='snippet,contentDetails,statistics,id',
        forUsername=channel_name
    ).execute()
    channel = {}
    results_count = response.get('pageInfo', {}).get('totalResults')
    if results_count == 1:
        channel = response['items'][0]
    else:
        logging.warning(
            f'Failed to get channel. Returned channels should be only one. '
            f'Got "{results_count}".'
        )

    next_page_token = None
    while True:
        response = youtube.search().list(
            part='snippet',
            channelId=channel.get('id'),
            maxResults=50,
            pageToken=next_page_token
        ).execute()
        next_page_token = response.get('nextPageToken')
        video_ids = [
            item['id']['videoId'] for item in response['items']
            if item['id']['kind'] == 'youtube#video'
        ]
        response = youtube.videos().list(
            part='snippet,contentDetails,statistics',
            id=','.join(video_ids),
            maxResults=50
        ).execute()
        for item in response['items']:
            if item['kind'] != 'youtube#video':
                continue
            snippet = item['snippet']
            tags = []
            for tag in snippet.get('tags', []):
                tag_instance, created = models.Tag.objects.get_or_create(
                    name=tag
                )
                tags.append(tag_instance)
            channel_instance, created = models.Channel.objects.get_or_create(
                identifier=snippet['channelId'],
                defaults={'name': snippet['channelTitle']}
            )
            published = timezone.make_aware(
                timezone.datetime.fromisoformat(snippet['publishedAt'][:-1])
            )
            video_instance, created = models.Video.objects.get_or_create(
                identifier=item['id'],
                defaults={
                    'name': snippet['title'],
                    'description': snippet['description'],
                    'channel': channel_instance,
                    'published': published
                }
            )
            if created:
                video_instance.tags.add(*tags)
            statistics = item['statistics']
            models.Statistic.objects.create(
                video=video_instance,
                view_count=statistics['viewCount'],
                like_count=statistics.get('likeCount'),
                dislike_count=statistics.get('dislikeCount'),
                favorite_count=statistics.get('favoriteCount'),
                comment_count=statistics.get('commentCount')
            )
        if not next_page_token:
            break
