from datetime import timedelta

from django.db.models import Avg
from django.forms.models import model_to_dict  # This is DRF! https://www.youtube.com/watch?v=7qXXWHfJha4
from django.http import HttpResponseBadRequest
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.views.generic import View

from . import models


def video_to_dict(video):
    """
    Converts Vidoe instance to python dictionary.
    """
    video_dict = model_to_dict(video)
    video_dict['tags'] = [
        model_to_dict(tag) for tag in video_dict['tags']
    ]
    return video_dict


class ListVideosByTag(View):
    """
    List vidos by given tags.
    """
    def get(self, request):
        """
        Returns videos by tag.
        """
        tags = [
            tag.strip() for tag in request.GET.get('tags', '').split()
            if tag
        ]
        videos = []
        if tags:
            for video in models.Video.objects.filter(tags__name__in=tags):
                videos.append(video_to_dict(video))
        return JsonResponse({'videos': videos})


class YoutubeStatistic(View):
    """
    Provides views statistic for video for the last hour.
    """
    def get(self, request):
        """
        Returns video views performance.
        """
        video_id = request.GET.get('video-id', '').strip()
        if not video_id:
            return HttpResponseBadRequest('No video ID provided.')
        then = timezone.now() - timedelta(hours=1)

        video = get_object_or_404(
            models.Video.objects.prefetch_related('channel'),
            identifier=video_id
        )
        video_dict = video_to_dict(video)
        avarege_views = video.statistic_set \
            .filter(date__lte=then) \
            .aggregate(avarege_views=Avg('view_count'))
        video_dict.update(avarege_views)

        other_videos_on_chanel = models.Video.objects \
            .filter(channel__identifier=video.channel.identifier) \
            .exclude(identifier=video_id)
        other_videos = []
        for other_video in other_videos_on_chanel:
            avarege_views = other_video.statistic_set \
                .filter(date__lte=then) \
                .aggregate(avarege_views=Avg('view_count'))['avarege_views']
            other_video_dict = video_to_dict(other_video)
            other_video_dict.update({
                'performance': video_dict['avarege_views'] / avarege_views
            })
            other_videos.append(other_video_dict)
        return JsonResponse({'videos': {
            'requested': video_dict,
            'others': other_videos
        }})
