from django.urls import path

from . import views

urlpatterns = [
    path('videos/by-tags/', views.ListVideosByTag.as_view(), name='youtube-videos-by-tag'),
    path('statistic/', views.YoutubeStatistic.as_view(), name='youtube-statistic'),
]
