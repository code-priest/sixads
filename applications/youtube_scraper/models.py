from django.db import models


class Tag(models.Model):
    """
    Tag by which marked video.
    """
    name = models.CharField(max_length=255)


class Channel(models.Model):
    """
    Channel to which video was uploaded.
    """
    name = models.CharField(max_length=255)
    identifier = models.CharField(max_length=255)


class Video(models.Model):
    """
    Youtube video.
    """
    identifier = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    description = models.TextField()
    channel = models.ForeignKey(Channel, models.CASCADE)
    published = models.DateTimeField()
    tags = models.ManyToManyField(Tag)


class Statistic(models.Model):
    """
    Video statistic.
    """
    video = models.ForeignKey(Video, models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    view_count = models.IntegerField()
    like_count = models.IntegerField(null=True)
    dislike_count = models.IntegerField(null=True)
    favorite_count = models.IntegerField(null=True)
    comment_count = models.IntegerField(null=True)
