import os

from celery import Celery
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sixads.settings')

celery = Celery()
celery.config_from_object('django.conf:settings', namespace='CELERY')
celery.autodiscover_tasks()

celery.conf.CELERYBEAT_SCHEDULE = {
    'scrap_youtube': {
        'task': 'applications.youtube_scraper.celery_tasks.run_scraper',
        'schedule': settings.YOUTUBE_SCRAPER_DELAY
    },
}

@celery.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
