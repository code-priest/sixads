try:
    from .local import *
except ImportError:
    from django.conf import ImproperlyConfigured
    raise ImproperlyConfigured(
        'You need to create `local.py` module to define local parameters. Use '
        '`local.py.template` as start point.'
    )
