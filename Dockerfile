FROM python:3.7-alpine
ENV PYTHONUNBUFFERED 1
RUN apk update
RUN apk add --no-cache gcc g++ make python-dev mariadb-dev
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/
