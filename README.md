# README #

This is test task for sixads. It scraps youtube videos statistic and then
shows videos by provided tags or shows video performance.

### How to run it? ###

To run this project you will need:

* [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [docker-compose](https://docs.docker.com/compose/install/)

Clone project by running: `git clone https://bitbucket.org/code-priest/sixads.git`
to any convenient for you directory. Then go to newly created directory(e.g.
in *unix based systems `cd sixads`)

After this you will need to create local setting for project. For conveniency
please use `sixads/settings/local.py.template` as start point.

Then run `docker-compose up` to start MySQl, redis, celery and celery-beat.


### What is next? ###

Now you need to wait until scraper gets for you some data.

So you have got some gata from youtube. Now you can use API endpoint to get some
data. Currently available next endpoints:

* get videos by tags. `http://localhost:8000/youtube/videos/by-tags/?tags=some tag 1,some tag 2`
* video performance. `http://localhost:8000/youtube/statistic/?video-id=someVideoId`
